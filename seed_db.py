import asyncio
from time import sleep

from sqlalchemy.orm import sessionmaker
from sqlmodel.ext.asyncio.session import AsyncSession

from app.db import init_db, engine


async def init():
    print("Creating tables...")
    await init_db()

    print("Creating first user..")
    async_session = sessionmaker(
        engine, class_=AsyncSession, expire_on_commit=False
    )
    async with async_session() as session:
        await session.execute(
            "INSERT INTO public.\"user\" (username, email, password, id, access) VALUES ('test', 'test@test.com', 'cc03e747a6afbbcbf8be7668acfebee5', 1, 'full');")
        await session.commit()
    print('Database initialized!')
    return


asyncio.get_event_loop().run_until_complete(init())
