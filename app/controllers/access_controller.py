from fastapi import Request, HTTPException
from sqlalchemy.orm import Session

from app.controllers.auth_controller import AuthController
from app.services.user_service import UserService


class AccessController:
    # Проверка прав пользователя
    @staticmethod
    async def has_access(request: Request, db: Session, access: str):
        user_service = UserService()
        token = AuthController.get_request_token(request)
        current_user = await user_service.get_user_by_token(db, token)
        if access in ['read', 'update', 'add', 'delete'] and current_user.access == 'full':
            return True
        if access == 'read' and current_user.access == 'read':
            return True
        raise HTTPException(status_code=403, detail="Ошибка прав доступа")
