from datetime import datetime
from hashlib import md5

from fastapi import HTTPException, Request, Depends
from sqlalchemy import select
from sqlalchemy.orm import Session

from app import models
from app.db import get_session


class AuthController:
    # Получения хеша пароля
    def get_password_hash(self, password):
        return md5(password.encode('utf-8')).hexdigest()

    # Проверка токена из заголовка на его наличие в БД и время жизни
    @staticmethod
    async def get_token_header(request: Request, db: Session = Depends(get_session)):
        token = request.headers.get('Authorization')
        already_token = select(models.Token).where(models.Token.access_token == token,
                                                   models.Token.expires_at > (datetime.now()))
        result = await db.execute(already_token)
        already_token = result.scalars().first()
        if already_token is None:
            raise HTTPException(status_code=400, detail="Сессия истекла")
        return True

    # Получения токена из заголовка
    @staticmethod
    def get_request_token(request: Request):
        token = request.headers.get('Authorization')
        return token

    # Генерация хеша авторизации из хеша пароля
    @staticmethod
    def create_token_header(hash_password: str):
        return md5(hash_password.encode('utf-8')).hexdigest()


