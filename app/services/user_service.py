from datetime import datetime, timedelta

from fastapi import HTTPException, Request
from sqlalchemy import delete
from sqlalchemy.exc import NoResultFound, IntegrityError
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from app import models
from app.controllers.auth_controller import AuthController
from app.schemas import UserCreate, UserUpdate


class UserService:
    auth_controller = AuthController()

    # Получения пользователя по логин и паролю
    async def get_user_by_username_and_password(self, db: Session, username, password):
        hash_password = self.auth_controller.get_password_hash(password)
        user = select(models.User).where(models.User.username == username, models.User.password == hash_password)
        result = await db.execute(user)
        try:
            user = result.scalars().one()
        except NoResultFound:
            raise HTTPException(status_code=400, detail="Пользователь не найден")
        token = AuthController.create_token_header(hash_password)
        already_token = select(models.Token).where(models.Token.access_token == token,
                                                   models.Token.expires_at > (datetime.now() + timedelta(hours=1)))
        result = await db.execute(already_token)
        try:
            already_token = result.scalars().one()
            return user, {'token': already_token.access_token, 'expires_at': already_token.expires_at}
        except NoResultFound:
            await db.execute(delete(models.Token).where(models.Token.user_id == user.id))
        expires_at = (datetime.now() + timedelta(hours=25))
        access_token = models.Token(access_token=token, user_id=user.id, expires_at=expires_at)
        db.add(access_token)
        await db.commit()
        return user, {'token': token, 'expires_at': expires_at}

    # Получение пользователя по токену авторизации
    async def get_user_by_token(self, db: Session, token: str):
        # Исключительно для показа работы с "сырыми" запросами
        result = await db.execute(
            'SELECT "user".id as id, username, email, access FROM "user" JOIN "token" ON "token".user_id = "user".id where "token".access_token = :token LIMIT 1',
            {'token': token})
        return models.User(**result.one_or_none())

    # Получение всех пользователей
    async def get_all(self, db: Session):
        user = select(models.User)
        result = await db.execute(user)
        users = result.scalars().all()
        return users or []

    # Создание пользователя
    async def create(self, db, user: UserCreate):
        user.password = self.auth_controller.get_password_hash(user.password)
        created_user = models.User(**user.__dict__)
        try:
            db.add(created_user)
            await db.commit()
        except IntegrityError:
            raise HTTPException(status_code=400, detail="Пользователь с таким логином уже существует")
        return created_user

    # Обновление пользователя
    async def update(self, user_id, db, user: UserUpdate, request: Request):
        current_user = await self.get_user_by_token(token=AuthController.get_request_token(request), db=db)
        if current_user.id == user_id:
            raise HTTPException(status_code=400, detail="Нельзя редактировать самого себя")
        update_user = select(models.User).where(models.User.id == user_id)
        result = await db.execute(update_user)
        update_user = result.scalar_one()
        if update_user is None:
            raise HTTPException(status_code=400, detail="Пользователь не найден")
        for key, value in user.dict().items():
            setattr(update_user, key, value)
        await db.commit()
        return update_user

    # Удаление пользователя
    async def delete(self, user_id, db, request: Request):
        current_user = await self.get_user_by_token(token=AuthController.get_request_token(request), db=db)
        if current_user.id == user_id:
            raise HTTPException(status_code=400, detail="Нельзя удалить самого себя")
        await db.execute(delete(models.Token).where(models.Token.user_id == user_id))
        await db.execute(delete(models.User).where(models.User.id == user_id))
        await db.commit()
        return
