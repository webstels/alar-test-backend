from datetime import datetime

from pydantic import BaseModel
from sqlalchemy import Column, String
from sqlmodel import Field, SQLModel


class TokenBase(SQLModel):
    access_token: str
    expires_at: datetime


class Token(TokenBase, table=True):
    id: int = Field(default=None, primary_key=True)
    user_id: int = Field(default=None, foreign_key="user.id", nullable=False)


class UserBase(SQLModel):
    username: str = Field(sa_column=Column("username", String, unique=True))
    email: str
    password: str
    access: str


class User(UserBase, table=True):
    id: int = Field(default=None, primary_key=True)

class UserLogin(BaseModel):
    username: str
    password: str
