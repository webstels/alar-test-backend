from pydantic import BaseModel


class UserCreate(BaseModel):
    username: str
    email: str
    password: str
    access: str


class UserUpdate(BaseModel):
    username: str
    email: str
    access: str
