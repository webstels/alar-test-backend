from fastapi import APIRouter, Depends, Request
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.controllers.access_controller import AccessController
from app.controllers.auth_controller import AuthController
from app.db import get_session
from app.schemas import UserCreate, UserUpdate
from app.services.user_service import UserService

router = APIRouter(
    prefix="/api",
    tags=["users"],
    dependencies=[Depends(AuthController.get_token_header)],
    responses={404: {"description": "Not found"}},
)

user_service = UserService()

# Список пользователей
@router.get("/user/")
async def get(request: Request, db: Session = Depends(get_session)):
    await AccessController.has_access(request, db, 'read')
    users = await user_service.get_all(db)
    return {'data': [jsonable_encoder(user) for user in users]}

# Добавление пользователя
@router.post("/user/")
async def post(request: Request, user: UserCreate, db: Session = Depends(get_session)):
    await AccessController.has_access(request, db, 'add')
    created_user = await user_service.create(db, user)
    return {'data': jsonable_encoder(created_user)}

# Редактирование пользователя
@router.put("/user/{user_id}/")
async def put(request: Request, user_id: int, user: UserUpdate, db: Session = Depends(get_session)):
    await AccessController.has_access(request, db, 'update')
    updated_user = await user_service.update(user_id, db, user, request)
    return {'data': jsonable_encoder(updated_user)}

# Удаление пользователя
@router.delete("/user/{user_id}/")
async def delete(request: Request, user_id: int, db: Session = Depends(get_session)):
    await AccessController.has_access(request, db, 'delete')
    await user_service.delete(user_id, db, request)
    return {'data': {'msg': 'Пользователь успешно удален'}}
