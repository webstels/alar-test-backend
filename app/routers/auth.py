from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app.db import get_session
from app.models import UserLogin
from app.services.user_service import UserService

router = APIRouter(
    prefix="/api/auth",
    tags=["auth"],
    responses={404: {"description": "Not found"}},
)

# Авторизация
@router.post("/login/")
async def login(body: UserLogin, db: Session = Depends(get_session)):
    user_service = UserService()
    user, token = await user_service.get_user_by_username_and_password(db, username=body.username,
                                                                       password=body.password)
    if user is None:
        raise HTTPException(status_code=400, detail="Пользователь не найден")
    return {"token": token['token'], "expires_at": token['expires_at']}
