import asyncio
import json

import grequests
from fastapi import APIRouter

router = APIRouter(
    prefix="/api/async",
    tags=["async"],
    responses={404: {"description": "Not found"}},
)


# Тестовый список №1
@router.get("/list_1/")
async def list_1():
    return [
        {'id': 1, 'text': 'Test 1'},
        {'id': 2, 'text': 'Test 2'},
        {'id': 3, 'text': 'Test 3'},
        {'id': 4, 'text': 'Test 4'},
        {'id': 5, 'text': 'Test 5'},
        {'id': 6, 'text': 'Test 6'},
        {'id': 7, 'text': 'Test 7'},
        {'id': 8, 'text': 'Test 8'},
        {'id': 9, 'text': 'Test 9'},
        {'id': 10, 'text': 'Test 10'},
        {'id': 31, 'text': 'Test 31'},
        {'id': 32, 'text': 'Test 32'},
        {'id': 33, 'text': 'Test 33'},
        {'id': 34, 'text': 'Test 34'},
        {'id': 35, 'text': 'Test 35'},
        {'id': 36, 'text': 'Test 36'},
        {'id': 37, 'text': 'Test 37'},
        {'id': 38, 'text': 'Test 38'},
        {'id': 39, 'text': 'Test 39'},
        {'id': 40, 'text': 'Test 40'},
    ]

# Тестовый список №2
@router.get("/list_2/")
async def list_2():
    return [
        {'id': 11, 'text': 'Test 11'},
        {'id': 12, 'text': 'Test 12'},
        {'id': 13, 'text': 'Test 13'},
        {'id': 14, 'text': 'Test 14'},
        {'id': 15, 'text': 'Test 15'},
        {'id': 16, 'text': 'Test 16'},
        {'id': 17, 'text': 'Test 17'},
        {'id': 18, 'text': 'Test 18'},
        {'id': 19, 'text': 'Test 19'},
        {'id': 20, 'text': 'Test 20'},
        {'id': 41, 'text': 'Test 41'},
        {'id': 42, 'text': 'Test 42'},
        {'id': 43, 'text': 'Test 43'},
        {'id': 44, 'text': 'Test 44'},
        {'id': 45, 'text': 'Test 45'},
        {'id': 46, 'text': 'Test 46'},
        {'id': 47, 'text': 'Test 47'},
        {'id': 48, 'text': 'Test 48'},
        {'id': 49, 'text': 'Test 49'},
        {'id': 50, 'text': 'Test 50'},
    ]

# Тестовый список №3
@router.get("/list_3/")
async def list_3():
    # Имитация долгой загрузки
    await asyncio.sleep(3)
    return [
        {'id': 21, 'text': 'Test 21'},
        {'id': 22, 'text': 'Test 22'},
        {'id': 23, 'text': 'Test 23'},
        {'id': 24, 'text': 'Test 24'},
        {'id': 25, 'text': 'Test 25'},
        {'id': 26, 'text': 'Test 26'},
        {'id': 27, 'text': 'Test 27'},
        {'id': 28, 'text': 'Test 28'},
        {'id': 29, 'text': 'Test 29'},
        {'id': 30, 'text': 'Test 30'},
        {'id': 51, 'text': 'Test 51'},
        {'id': 52, 'text': 'Test 52'},
        {'id': 53, 'text': 'Test 53'},
        {'id': 54, 'text': 'Test 54'},
        {'id': 55, 'text': 'Test 55'},
        {'id': 56, 'text': 'Test 56'},
        {'id': 57, 'text': 'Test 57'},
        {'id': 58, 'text': 'Test 58'},
        {'id': 59, 'text': 'Test 59'},
        {'id': 60, 'text': 'Test 60'},
    ]


@router.get("/list/")
async def async_list():
    requests = [
        grequests.get('http://127.0.0.1:8001/api/async/list_1/', timeout=2),
        grequests.get('http://127.0.0.1:8001/api/async/list_2/', timeout=2),
        grequests.get('http://127.0.0.1:8001/api/async/list_3/', timeout=2),

    ]
    responses = grequests.map(requests, exception_handler=exception_handler)
    data = (item for response in responses if response is not None and response.status_code == 200 for item in
            json.loads(response.text))
    sorted_list = sorted(data, key=lambda d: d['id'])
    return {'data': sorted_list}


def exception_handler(request, exception):
    print('Request timeout')
