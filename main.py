from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from app.db import init_db
from app.routers import auth, user, async_requests

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
    allow_credentials=True,
)
app.include_router(auth.router)
app.include_router(user.router)
app.include_router(async_requests.router)


@app.on_event("startup")
async def on_startup():
    await init_db()


@app.get("/")
async def root():
    return {"message": "Hello World"}
